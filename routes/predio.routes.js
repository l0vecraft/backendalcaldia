const express = require('express');
const router = express.Router();
const predioController = require('../controllers/predioController');

router.post('/predio/create/:id',predioController.create);
router.get('/predios/list',predioController.list);

module.exports = router;