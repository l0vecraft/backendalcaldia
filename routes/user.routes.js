const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/userController');

router.post('/user/create',userCtrl.create);
router.get('/user/list',userCtrl.list);
router.route('/user/:id')
    .get(userCtrl.getUser)
    .post(userCtrl.update);

module.exports = router;