const mongoose = require("mongoose");
const predioModel = require('./predio');
//funcion realizada solo para asignar una longitud
var lenght = (minLeng, maxLeng) => {
  minLeng = minLeng || 0;
  maxLeng = maxLeng || Infinity;
  return {
    validator: (value) => {
      if (value === undefined) return true;
      return value.lenght >= minLeng && value.lenght <= maxLeng;
    },
    message:
      "Este se campo debe contener un numero entre" +
      minLeng +
      " y " +
      maxLeng +
      " de caracteres.",
  };
};
const Predio = mongoose.Schema({
  idUsuario:String,
  departamento: {
    type: String,
    uppercase:true
  },
  municipio: {
      required: true,
      type: String,
      uppercase:true
    },
    codigo:{
        type: Number,
        validator: lenght(5,5)
    },
    nombre: {
      type:String,
      uppercases:true
    },
  //   lng:Numbers, esta es la longitud
  // lat: Number, esta es la latitud
  contacto: Number,
  actividad:{
    type:String,
    uppercase:true
  },
  linea:{
    type: String,
    uppercase:true
  },
  hectareas: Number
});


const User = new mongoose.Schema({
  fechaAct: Date,
  vigencia: {
    type: String, //supongo que es una fecha fija
    default: "2020",
  },
  tipoDoc: {
    required: true,
    type: String,
    uppercase:true
  },
  numDoc: {
    required: true,
    type: String,
    validator: lenght(8, 11),
  },
  pNombre: {
    required: true,
    type: String,
    uppercase:true
  },
  sNombre: String,
  pApellido: {
    required: true,
    type: String,
    uppercase:true
  },
  sApellido: {
    required: true,
    type: String,
    uppercase:true
  },
  fNacimiento: {
    required: true,
    type: Date,
  },
  sexo: String,
  etnia: {
    required: true,
    type: String,
    uppercase:true
  },
  discapacidad: Boolean,
  predios:[Predio]
});


module.exports = mongoose.model("User", User)
