const mongoose = require('mongoose');
const userModel = require("./user");
//funcion realizada solo para asignar una longitud
var lenght = (minLeng, maxLeng) => {
    minLeng = minLeng || 0;
    maxLeng = maxLeng || Infinity;
    return {
      validator: (value) => {
        if (value === undefined) return true;
        return value.lenght >= minLeng && value.lenght <= maxLeng;
      },
      message:
        "Este se campo debe contener un numero entre" +
        minLeng +
        " y " +
        maxLeng +
        " de caracteres.",
    };
  };

const Predio = mongoose.Schema({
    idUsuario:String,
    departamento: {
      type: String,
      uppercase:true
    },
    municipio: {
        required: true,
        type: String,
        uppercase:true
      },
      codigo:{
          type: Number,
          validator: lenght(5,5)
      },
      nombre: {
        type:String,
        uppercase:true
      },
    //   lng:Numbers, esta es la longitud
    // lat: Number, esta es la latitud
    contacto: Number,
    actividad:{
      type:String,
      uppercase:true
    },
    linea:{
      type: String,
      uppercase:true
    },
    hectareas: Number
});
module.exports = mongoose.model('Predio',Predio);