const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const URI = 'mongodb://localhost:27017/alcaldia';//esto puede modificarse en un futuro
mongoose.connect(URI,{useNewUrlParser:true, useUnifiedTopology:true}).then(()=>{
    console.log('Corriendo DB');
}).catch(err=>console.error(err));
module.exports = mongoose;