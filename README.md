# backendAlcaldia
## Instalacion
una vez se clone el repositorio, entre en el directorio y ejecute el comando `npm install`.

## Dependecias
las siguientes fueron las dependencias instaladas para el desarrollo del proyecto
las pueden encontrar en el archivo `package.json`.

Inline-style: 
* **Cors**: 2.8.5, para manejar el intercambio de recursos de los servidores
* **Express**: 4.17.1, framework para desarrollar todo el backend
*  **Mongoose**: 5.9.9, el conector de la base de datos no relacional en este caso MongoDB
*  **Morgan**: 1.10.0, esta libreria sirve para regresarnos los codigos de estado de las
*  peticiones que se realiazan a nuestro servidor
*  **Nodemon**: 2.0.3, para el recargado automatico del servidor durante el desarrollo

## Estructura de carpetas
la estructura de carpetas que se utilizo durante del desarrollo fue el siguiente
1. **assets**, fue el lugar desigando para guardar imagenes que servirian para ilustras este README.md
2. **controllers**, lugar donde se almacenan los metodos de cada una de las entidades
    * **usuarioController** 
    * **prediosController**
3. **modelos**, lugar donde se almacen los modelos para las clases
    * **usuarioModel**
    * **prediosModel**
4. **node_modules**, donde se guardan los paquetes instalados
5. **routes**, aqui se encuentran las rutas para acceder a nuestra api
    * **usuario.routes**
    * **predios.routes**
6. **App.js**, es nuestro servidor completo
7. **indexj.js**, la conexion de base de datos

## Estructura del servidor
una vez entramos al archivo `app.js` nos toparemos con varias lineas de importaciones
y muchas lineas de codigo las cuales estan divididas en:
1. la asignacion de un puerto por defecto en caso de que no exista uno dado por el servidor
2. donde se ejecute la aplicacion `app.set('port',process.env.PORT||PORT)` donde **PORT**
3. en este caso es 5000
4. **middlewears**, los cuales estaran a la escucha de cada peticion , aqui encontramos
    * `morgan('dev')`, esto con el fin de que solo funciones en desarrollo, no en produccion
    * `express.json()`, para que tanto peticiones como respuestas sean en formato **json**
    * `cors()`, para que no exista problema en el intercambio de recursos entre servidores (front-back)
5. **server**, este espacio esta reservado solo para desplegar el servidor que esta escuchando
                el purto antes mencionado, luego nos regresara la direccion donde se esta ejecutanto la aplicacion
6. **archivos de ruta**. aqui estaran los archivos que contienen las rutas de cada uno de las entidades, con una ruta en comun por defecto `/api`
7. **statics**, la cual sera la ruta de los ficheros estaticos de la aplicacion, es decir donde se guardaran imagenes que se suban a la app o archivos.