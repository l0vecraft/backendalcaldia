const express = require('express');
const path = require('path');
const cors = require('cors');
const DB = require('./index');
const userRoutes = require('./routes/user.routes');
const predioRoutes = require('./routes/predio.routes');
const morgar = require('morgan');
const app = express();

const PORT = 5000;
app.set('port',process.env.PORT||PORT);

//middles
app.use(morgar('dev'));
app.use(express.json());
app.use(cors());
//server
app.listen(app.get('port'),()=>{
    console.log('Server corriendo en http://localhost:'+app.get('port'));
})
//archivos de rutas
app.use('/api',userRoutes);
app.use('/api',predioRoutes);
//statics
app.use('/dist',express.static(path.join(__dirname,'dist')));

module.exports = app;