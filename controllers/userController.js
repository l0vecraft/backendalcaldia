const userModel = require("../models/user");

let userController = {
  create: (req, res, next) => {
      let params = req.body;
      let newUser = new userModel();
      newUser.tipoDoc = params.tipoDoc||'';
      newUser.fechaAct = Date.now();
      newUser.numDoc = params.numDoc||10000000;
      newUser.pNombre = params.pNombre||'';
      newUser.sNombre = params.sNombre||'';
      newUser.pApellido = params.pApellido||'';
      newUser.sApellido = params.sApellido||'';
      newUser.fNacimiento = params.fNacimiento||null;//año-mes-dia
      newUser.sexo = params.sexo||'';
      newUser.etnia = params.etnia||'';
      newUser.discapacidad = params.discapacidad||false;
      newUser.predio = params.predio||[];
      newUser.save((err,result)=>{
          if(err) return res.status(500).json('Error en el servidor');
          return res.status(200).json(result);
      })
  },
  getUser: (req, res) => {
      let id = req.params.id;
      if(!id|| id==='') return res.status(400).json("User doesn't found.");
      userModel.findById(id,(err,user)=>{
          if(err) return res.status(404).json('Algo salio mal.');
          return res.status(200).json(user);
      })
  },
  list: (req, res) => {
      userModel.find((err,users)=>{
        if(err) return res.status(500).json('Error en el servidor.');
        if(!users || users.length==0) return res.status(404).json('Aun no se encontraron usuarios :(');
        return res.status(200).json({"consecutivo":users.length,"users":users});
      });
  },
  update: (req, res) => {
      let id = req.params.id;
      let update = req.body;
      if(!id|| id==='') return res.status(400).json("User doesn't found.");
      userModel.findByIdAndUpdate(id,update,{new:true},(err,user)=>{
          if(err) return res.status(500).json('Error en el servidor');
          if(!user) return res.status(404).json('Algo salio mal.')
          return res.status(200).json(user);
      })
  },
};
module.exports = userController;