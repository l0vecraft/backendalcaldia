const predioModel = require("../models/predio");
const userModel = require("../models/user");

const predioController = {
  create: (req, res,next) => {
    var newPredio = predioModel();
    var params = req.body;
    if (
      req.params.id !== "" &&
      req.params.id !== undefined &&
      req.params.id !== null
    ) {
      newPredio.idUsuario = req.params.id;
      newPredio.departamento = params.departamento;
      newPredio.municipio = params.municipio;
      newPredio.codigo = params.codigo;
      newPredio.nombre = params.nombre;
      newPredio.contacto = params.contacto;
      newPredio.actividad = params.actividad;
      newPredio.linea = params.linea;
      newPredio.hectareas = params.hectareas;
      var user=userModel.findById(newPredio.idUsuario,(err,usuario)=>{
        if(err) return res.status(500).json('Error en el servidor, no se pudo realizar la accion con el usuario');
        if(!usuario) return res.status(404).json('Usuario no encontrado o inexistente');
        usuario.predios.push(newPredio);
            usuario.save((err)=>{
              if(err) console.error('Error al acutalizar el usuario: '+err);
              newPredio.save((err,result)=>{
              if(err) return res.status(500).json('Error en el servidor');
              // })
              return res.status(200).json(result);
              });
        })
      });
    }
  },
  list:(req,res)=>{
    predioModel.find((err,predios)=>{
      if(err) return res.status(500).json('Error en el servidor');
      if(!predios) return res.status(404).json('No se encontraron predios disponibles');
      return res.status(200).json(predios);
    })
  }
};

module.exports = predioController;
